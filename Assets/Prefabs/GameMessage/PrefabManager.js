﻿#pragma strict

public var FADE_TIME : double = 0.5;
public var autoshow : boolean = true;

var img : UnityEngine.UI.Image;
var txt : UnityEngine.UI.Text;

private var destroying : boolean = false;

var hideKey : KeyCode = KeyCode.Escape;

private var showing : boolean = false;

var autohideInSeconds : float = -1.0f;

function Start () {
	img.CrossFadeAlpha(0, 0, false);
	txt.CrossFadeAlpha(0, 0, false);
	
	if (autoshow) {
		show();
	}
}

function show() {
	destroying = false;
	
	img.CrossFadeAlpha(1, FADE_TIME, false);
	txt.CrossFadeAlpha(1, FADE_TIME, false);
	
	yield WaitForSeconds(FADE_TIME);
	showing = true;
	
	if (autohideInSeconds > 0.0f) {
		yield WaitForSecondsRealtime(autohideInSeconds);
		hide();
	}
}

function hide() {
	if (!destroying) {
		destroying = true;
		
		img.CrossFadeAlpha(0, FADE_TIME, false);
		txt.CrossFadeAlpha(0, FADE_TIME, false);
		
		yield WaitForSeconds(FADE_TIME);
		destroying = false;
		showing = false;
	}
}

function isShowing() {
	return showing;
}

function isBeingDestroyed() : boolean {
	return destroying;
}

//public static function hideOne() {
	//for (var go : GameObject in GameObject.FindGameObjectsWithTag("GameMessage")) {
	//	if (go.activeInHierarchy) {
	//		var pm : PrefabManager = go.GetComponentInChildren.<PrefabManager>();
	//		if (pm.isShowing()) {
	//			Debug.Log(go.name + " is showing. Hiding now.");
	//			pm.hide();
	//			break;
	//		}
	//	}
//	}
//}

function Update() {
	if(Input.GetKeyDown(hideKey)) {
		hide();
	}
}