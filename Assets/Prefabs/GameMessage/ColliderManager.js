﻿#pragma strict

var messagePrefab : GameObject;

var hideOnExit : boolean = true;

private var prefabManager : PrefabManager;

function Start() {
	var go = Instantiate(messagePrefab, transform.position, Quaternion.identity);
	prefabManager = go.GetComponent.<PrefabManager>();
}

function OnTriggerEnter(collider : Collider) {
	if (collider.gameObject.tag == "Player") {
		prefabManager.show();
	}
}

function OnTriggerExit(collider : Collider) {
	if (hideOnExit && collider.gameObject.tag == "Player") {
		prefabManager.hide();
	}
}