﻿#pragma strict

@script RequireComponent(AudioSource)

function Start () {
	var clip : AudioClip = gameObject.GetComponent.<AudioSource>().clip;

	Destroy(gameObject, null == clip ? 0 : clip.length);
}

function Update () {

}