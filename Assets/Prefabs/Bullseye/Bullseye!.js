﻿#pragma strict

var max_tracked_collisions : int = 1000000;
var bullseyeListeners : GameObject[];
var eventName : String = "onBullseye";

private var num_collisions : int = 0;

function OnCollisionEnter (collision : Collision) {
	if (num_collisions < max_tracked_collisions && collision.gameObject.tag == "DamageDealer") {
		++num_collisions;
		Debug.Log("Got a bullseye");
		for (var listener : GameObject in bullseyeListeners) {
			Debug.Log("Sending to " + listener.name);
			listener.SendMessage(eventName, SendMessageOptions.DontRequireReceiver);
		}
	} else {
		Debug.Log(num_collisions + "reached. Collider ignored.");
	}
}