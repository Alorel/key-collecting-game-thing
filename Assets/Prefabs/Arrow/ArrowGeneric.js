﻿#pragma strict

public class ArrowGeneric extends MonoBehaviour {
	protected var flying : boolean = true;

	protected function doCollisionHandling(col : GameObject) {
		if (col.tag == "ProjectileDestroy" || col.tag == "Ocean" || null != col.GetComponent.<HealthHaver>()) {
			Destroy(gameObject);
		} else {
			Destroy(gameObject, 2);
		}
	}

	function OnCollisionEnter (collision : Collision) {
		Debug.Log("Collision with " + collision.gameObject.name);
		if (flying) {
			flying = false;
			doCollisionHandling(collision.gameObject);
		}
	}

	function OnTriggerEnter (collision : Collider) {
		Debug.Log("Trigger with " + collision.gameObject.name);
		if (flying) {
			flying = false;
			doCollisionHandling(collision.gameObject);
		}
	}

	function FixedUpdate () {
	     if (flying) {
			 transform.LookAt (transform.position + GetComponent.<Rigidbody>().velocity);
		 }
	 };
}