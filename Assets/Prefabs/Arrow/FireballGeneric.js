﻿#pragma strict

public class FireballGeneric extends ArrowGeneric {

	public var initSound : AudioClip;
	public var playNDestroyPrefab : GameObject;

	function Start() {
		var audio : AudioSource = gameObject.AddComponent.<AudioSource>();
		audio.spatialBlend = 1f;
		audio.clip = initSound;
		audio.minDistance = 5;
		audio.priority = 0;

		audio.Play();
	}

	protected function doCollisionHandling(collision : GameObject) {
		Instantiate(playNDestroyPrefab, gameObject.transform.position, Quaternion.identity);

		Destroy(gameObject);
	}
}