﻿#pragma strict

public class MeteorGeneric extends ArrowGeneric {
	public var playNDestroyPrefab : GameObject;

	function Start() {
		Destroy(gameObject, 5);
	}

	protected function doCollisionHandling(col : GameObject) {
		Instantiate(playNDestroyPrefab, gameObject.transform.position, Quaternion.identity);

		Destroy(gameObject);
	}

	function FixedUpdate () {
	     //Don't do transformations
	 };
}