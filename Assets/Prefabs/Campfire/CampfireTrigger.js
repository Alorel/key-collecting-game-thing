﻿#pragma strict

var burnSound : AudioClip;

private var burnSFX : AudioSource;

function Start() {
		burnSFX = gameObject.AddComponent(AudioSource);
		burnSFX.clip = burnSound;
		burnSFX.playOnAwake = false;
}

function OnTriggerEnter (collider : Collider) {
    if (collider.gameObject.tag == "Player") {
		collider.gameObject.GetComponent.<HealthHaver>().triggerDamage();
		burnSFX.Play();
    }
}