﻿#pragma strict

function hide() {
	var renderers : Renderer[] = GetComponentsInChildren.<Renderer>();
	for (var renderer : Renderer in renderers) {
		renderer.enabled = false;
	}
	
	var colliders : Collider[] = GetComponentsInChildren.<Collider>();
	for (var collider : Collider in colliders) {
		collider.enabled = false;
	}
	
	var halo : Component = GetComponent("Halo");
	if (null != halo) {
		halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
	}
}

public static function manuallyRegisterWithGSM() {
	GameObject.Find("GameState").GetComponent.<GameStateManager>().keys_in_scene++;
}

function OnTriggerEnter(collider : Collider) {
	if (collider.gameObject.tag == "Player") {
		GameObject.Find("GameState").GetComponent.<GameStateManager>().addKey();
		
		var audio : AudioSource = GetComponent.<AudioSource>();
		audio.Play();
		
		hide();
		Destroy(gameObject, audio.clip.length);
	}
}