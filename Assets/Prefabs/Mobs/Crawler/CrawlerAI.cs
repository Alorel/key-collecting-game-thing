﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(MoveTowardsPlayer))]
[RequireComponent(typeof(TurnAtPlayer))]
[RequireComponent(typeof(Collider))]
public class CrawlerAI : MonoBehaviour {

	public float attackRate = 2f;

	private Animator animator;

	private GameObject player;

	private float timeSinceLastAttack = 7200f;

	void Start() {
		animator = GetComponent<Animator> ();
		player = GameObject.FindWithTag ("Player");

		Physics.IgnoreCollision (GetComponent<Collider> (), player.GetComponent<Collider> ());
	}

	void onDeath() {
		gameObject.GetComponent<MoveTowardsPlayer> ().enabled = false;
		gameObject.GetComponent<TurnAtPlayer> ().enabled = false;
		gameObject.GetComponent<Collider> ().enabled = false;
		animator.enabled = false;
		foreach (MeshRenderer mr in gameObject.GetComponentsInChildren<MeshRenderer>()) {
			mr.enabled = false;
		}

		Destroy (gameObject, 1.5f);
	}

	void attacking() {
		timeSinceLastAttack = 0f;
		player.SendMessage ("triggerDamage");
	}

	void targetReachedPlayer(GameObject crawler) {
		if ((timeSinceLastAttack += Time.fixedDeltaTime) > attackRate) {
			animator.SetTrigger ("attack");
		}
	}
}
