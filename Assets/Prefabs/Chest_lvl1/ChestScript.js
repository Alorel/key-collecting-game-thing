﻿#pragma strict

private var gsm : GameStateManager;

var toastPrefab : GameObject;
var successCanvas : GameObject;
var openChestPrefab : GameObject;

private static var min_distance : float = 1.36;

function Start () {
	gsm = GameObject.Find("GameState").GetComponent.<GameStateManager>();
}

function onInteraction(rc : RaycastHit) {
	if (gsm.keys_in_scene > gsm.keys_held) {
		toast("I don't have enough keys");
	} else {
		Instantiate(openChestPrefab);
		successCanvas.SetActive(true);
		Destroy(GameObject.FindWithTag("PauseMenu"));
		Destroy(gameObject);
	}
}

private function toast(msg:String) {
	Instantiate(toastPrefab, transform.position, Quaternion.identity).GetComponent.<MessageSetter>().setMessage(msg);
}