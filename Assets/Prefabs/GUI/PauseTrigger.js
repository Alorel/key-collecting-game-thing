﻿#pragma strict

var triggerKey : KeyCode  = KeyCode.Escape;

var pauseCanvas : GameObject;

private var isCurrentlyActive : boolean;

function Start() {
	isCurrentlyActive = pauseCanvas.activeInHierarchy;
}

// Update is called once per frame
function Update () {
	if (Input.GetKeyDown(triggerKey)) {
		isCurrentlyActive = !isCurrentlyActive;
		pauseCanvas.SetActive(isCurrentlyActive);
	}
}