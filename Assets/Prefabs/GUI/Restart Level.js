﻿#pragma strict

import UnityEngine.SceneManagement;

function onSelect(itemName : String) {
	if (itemName == gameObject.name) {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}
}