﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuButton : MonoBehaviour {
	
	public string mainMenuSceneName = "home_screen";

	void onSelect(string itemName) {
		if (itemName == gameObject.name) {
			Debug.Log("MainMenu button clicked - loading appropriate scene");
			SceneManager.LoadScene(mainMenuSceneName, LoadSceneMode.Single);
		}
	}
}
