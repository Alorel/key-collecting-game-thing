﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoadingMenuItem : MonoBehaviour {

	public string level;

	void onSelect(string itemName) {
		if (itemName == gameObject.name) {
			PlayerPrefs.DeleteKey("level_needs_reload");
			SceneManager.LoadScene(level, LoadSceneMode.Single);
		}
	}
}
