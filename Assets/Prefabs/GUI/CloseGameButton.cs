﻿using UnityEngine;
using System.Collections;

public class CloseGameButton : MonoBehaviour {

	void onSelect(string itemName) {
		if (itemName == gameObject.name) {
			Debug.Log("Close game button clicked - quitting application");
			Application.Quit();
		}
	}
}
