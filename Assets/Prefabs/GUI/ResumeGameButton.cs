﻿using UnityEngine;
using System.Collections;

public class ResumeGameButton : MonoBehaviour {

	void onSelect(string itemName) {
		if (itemName == gameObject.name) {
			gameObject.transform.parent.parent.parent.gameObject.SetActive(false);
		}
	}
}
