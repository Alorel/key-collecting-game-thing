﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {
	
	private List<GameObject> buttons = new List<GameObject>();
	
	public Color highlightedColor = Color.white;
	public Color notHighlightedColor = new Color(0.441f, 0.441f, 0.441f, 1f);
	
	public string onSelectMethod = "onSelect";
	public string onHighlightMethod = "onHighlight";
	
	public KeyCode[] nextItemKeys     = {KeyCode.DownArrow, KeyCode.RightArrow};
	public KeyCode[] previousItemKeys = {KeyCode.UpArrow, KeyCode.LeftArrow};
	public KeyCode[] selectKeys       = {KeyCode.Return, KeyCode.KeypadEnter};
	
	private int index;

	// Use this for initialization
	void Awake () {
		for (int i = 0; i < transform.childCount; i++) {
			buttons.Add(transform.GetChild(i).gameObject);
		}
		
		highlight(0);
	}
	
	private bool highlight(int buttonIndex) {
		if (buttonIndex < 0 || buttonIndex >= buttons.Count) {
			return false;
		} else {
			foreach (GameObject g in buttons) {
				g.GetComponent<Image>().color = notHighlightedColor;
			}
			
			buttons[buttonIndex].GetComponent<Image>().color = highlightedColor;
			index = buttonIndex;
			
			BroadcastMessage(onHighlightMethod, buttons[index], SendMessageOptions.DontRequireReceiver);
			
			return true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		foreach (KeyCode kk in nextItemKeys) {
			if (Input.GetKeyDown(kk)) {
				highlight(index < buttons.Count - 1 ? index + 1 : 0);
				return;
			}
		}
		foreach (KeyCode kk in previousItemKeys) {
			if (Input.GetKeyDown(kk)) {
				highlight(index > 0 ? index - 1 : buttons.Count - 1);
				return;
			}
		}
		foreach (KeyCode kk in selectKeys) {
			if (Input.GetKeyDown(kk)) {
				BroadcastMessage(onSelectMethod, buttons[index].name, SendMessageOptions.RequireReceiver);
				return;
			}
		}
	}
}
