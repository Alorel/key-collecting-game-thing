﻿#pragma strict

var saveUnavailableText : GameObject ;

var gameKeyPrefab : GameObject;

private var saveExists : boolean ;

function Start() {
	saveExists = SaveHelper.canLoad();
}

function onSelect(item : String) {
	if (gameObject.name == item) {
		new SaveHelper().load(gameKeyPrefab);
	}
}

function onHighlight(item : GameObject) {
	saveUnavailableText.SetActive(item.name == gameObject.name && !saveExists);
}

function Update() {
	if (Cursor.lockState != CursorLockMode.Locked) {
		Cursor.lockState = CursorLockMode.Locked;
	}
}