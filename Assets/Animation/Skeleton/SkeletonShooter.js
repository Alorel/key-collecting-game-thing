﻿#pragma strict

@script RequireComponent(Animator)
@script RequireComponent(HealthHaver)
public class SkeletonShooter extends Shooter {

	private var anim : Animator;
	public var shootDistanceThreshold : float = 17.5f;
	public var turnDistanceThreshold : float = 30f;

	@Tooltip("States in which the NPC won't be turning or shooting")
	public var idleInStates : String[] = ["Resurrection", "Death"];

	private var playerHealth : HealthHaver;

	function Start() {
		super.Start();
		playerHealth = player.GetComponent.<HealthHaver>();
		anim = gameObject.GetComponent.<Animator>();
	}

	protected function instantiateProjectile() : GameObject {
		var go : GameObject = Instantiate(
			projectilePrefab, 
			spawnLoc, 
			gameObject.transform.rotation
		) as GameObject;
		addCollisionIgnore(go);
		return go;
	}

	protected function get isInIdleState() : boolean {
		var currentState : AnimatorStateInfo = anim.GetCurrentAnimatorStateInfo(0);

		for (var name : String in idleInStates) {
			if (currentState.IsName(name)) {
				return true;
			}
		}

		return false;
	}

	protected function get shouldTurn() : boolean {
		return !isInIdleState && distance <= turnDistanceThreshold;
	}

	protected function get shouldShoot() : boolean {
		return can_shoot
				&& distance <= shootDistanceThreshold
				&& timeSinceLastShot >= shootFrequency
				&& !dead
				&& !playerHealth.dead
				&& !isInIdleState;
	}

	protected function get distance() : float {
		return Vector3.Distance(player.transform.position, gameObject.transform.position);
	}

	function Update() {
		if (shouldTurn) {
			gameObject.transform.LookAt(player.transform);
		}

		if (shouldShoot) {
			anim.SetTrigger("fire");
		}

		super.Update();
	}
}