﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class SkeletonDamageTaker : MonoBehaviour {

	private Animator animator;

	private GameObject torch;
	private GameObject ringOfFire;

	void Start() {
		animator = GetComponent<Animator> ();

		if (null != transform.parent) {
			Transform rof = transform.parent.Find ("RingOfFire");
			Transform torch = transform.parent.Find ("WallTorch");

			if (null != rof) {
				ringOfFire = rof.gameObject;
			}
			if (null != torch) {
				this.torch = torch.gameObject;
			}
		}
	}

	void onChange(int[] healthState) {
		animator.SetTrigger ("hit");
	}

	void onDeath() {
		animator.SetTrigger ("die");
	}

	private IEnumerator deathAnimationHandler(float waitTime) {
		yield return new WaitForSeconds (waitTime + 1.5f);
		BroadcastMessage ("deathAnimationFinished", null, SendMessageOptions.DontRequireReceiver);
	}

	void deathAnimationFinished() {
		if (null != torch) {
			Destroy (torch);
		}
		if (null != ringOfFire) {
			Destroy (ringOfFire);
		}
		Destroy (gameObject);
	}

	void deathAnimationStarted(float waitTime) {
		StartCoroutine (deathAnimationHandler(waitTime));
	}
}
