﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Toggler))]
public class TorchDisabler : MonoBehaviour {

	public GameObject lightGO;

	private Light light;

	private Toggler toggler;

	void Start() {
		light = lightGO.GetComponent<Light> ();
		toggler = gameObject.GetComponent<Toggler> ();
	}

	void disableLighting() {
		light.enabled = false;
		toggler.togglerEnabled = false;
	}

	void enableLighting() {
		enableLighting (true);
	}

	void enableLighting(bool turnLightOn) {
		light.enabled = turnLightOn;
		toggler.togglerEnabled = true;
	}
}
