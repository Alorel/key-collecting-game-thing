﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingPlatform : MonoBehaviour {

	private Dictionary<GameObject, Transform> parents = new Dictionary<GameObject,Transform>();

	public string[] ignoredTags = {"Terrain"};

	private bool ignore(Collider c) {
		foreach (string tag in ignoredTags) {
			if (c.gameObject.tag == tag) {
				return true;
			}
		}
		return false;
	}

	void OnTriggerStay(Collider o) {
		if (!ignore (o)) {
			tryAddParent (o.gameObject);
			o.gameObject.transform.parent = gameObject.transform;
		}
	}

	private void tryAddParent(GameObject child) {
		if (!parents.ContainsKey (child)) {
			parents.Add (child, child.transform.parent);
		}
	}

	void OnTriggerExit(Collider o) {
		if (!ignore (o)) {
			Transform previousParent;
			parents.TryGetValue (o.gameObject, out previousParent);

			o.gameObject.transform.parent = previousParent;
		}
	}
}