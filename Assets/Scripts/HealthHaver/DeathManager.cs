﻿using UnityEngine;
using System.Collections;

public class DeathManager : MonoBehaviour {
	
	public GameObject deathPrefab;
	
	void onDeath() {
		Destroy(GameObject.FindWithTag("PauseMenu"));
		Destroy(GameObject.FindWithTag("BGMusicManager"));
		Instantiate(deathPrefab);
	}
}
