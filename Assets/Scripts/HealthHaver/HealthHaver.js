﻿#pragma strict

@Header("Health Management")
var curr_health : int = 1;
var max_health : int = 1;

@Tooltip("Number of seconds it takes to regenerate one point of health")
@Range(-1f, 36000f)
var regenTime : float = -1f;

@Header("Event Management")
var deathTrigger : String = "onDeath";
var changeTrigger : String = "onChange";

@Header("Sound management")
var painSound : AudioClip;
var deathSound : AudioClip;

private var painSource : AudioSource;

protected var ignore = false;

private var timeSinceLastRegen : float = 0f;

public var immuneOverride : boolean = false;

public function get dead() : boolean {
	return curr_health <= 0;
}

function Start() {
	painSource = gameObject.AddComponent(AudioSource) as AudioSource;
	painSource.spatialBlend = 1f;
	painSource.clip = painSound;
	painSource.minDistance = 5;
	painSource.priority = 0;
}

public function get immune() : boolean {
	return immuneOverride;
}

function FixedUpdate() {
	if (regenTime != -1 && !dead && !ignore && curr_health < max_health) {
		timeSinceLastRegen += Time.deltaTime;

		if (timeSinceLastRegen > regenTime) {
			curr_health += 1;
			timeSinceLastRegen = 0f;
		}
	}
}

function triggerDamage() {
	if (!immune && !ignore) {
		Debug.Log(String.Format("Triggering damage on {0}", gameObject.name));

		if (--curr_health <= 0) {
			ignore = true;
			if (null != deathSound) {
				painSource.clip = deathSound;
			} 
			painSource.Play();
			BroadcastMessage(deathTrigger, null, SendMessageOptions.DontRequireReceiver);
		} else {
			painSource.Play();
			var msg : int[] = [curr_health, max_health];
			BroadcastMessage(changeTrigger, msg, SendMessageOptions.DontRequireReceiver);
		}
	} else {
		Debug.Log(String.Format("{0} is currently immune; ignoring damage", gameObject.name));
	}
}

function OnCollisionEnter (collision : Collision) {
	if (collision.gameObject.tag == "DamageDealer") {
		triggerDamage();
	}
}