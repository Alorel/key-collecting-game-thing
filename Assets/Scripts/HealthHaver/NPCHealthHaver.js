﻿#pragma strict

@script RequireComponent(Animator)
public class NPCHealthHaver extends HealthHaver {

	@Tooltip("Animation states where the NPC won't take damage")
	public var immuneAnimationStates : String[] = [];

	private var anim : Animator;

	function Start() {
		super.Start();
		anim = gameObject.GetComponent.<Animator>();
	}

	public function get immune() : boolean {
		if (immuneOverride) {
			return true;
		}

		var currentState : AnimatorStateInfo = anim.GetCurrentAnimatorStateInfo(0);

		for (var name : String in immuneAnimationStates) {
			if (currentState.IsName(name)) {
				Debug.Log("Anim state: " + name);
				return true;
			}
		}

		return false;
	}
}