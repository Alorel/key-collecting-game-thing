﻿#pragma strict

public class TransformSpec {
	
	var position : Vector3;
	var rotation : Quaternion;
	var localScale : Vector3;
	var parent : GameObject;
	
	function TransformSpec(tr : Transform) {
		position = tr.position;
		rotation = tr.rotation;
		localScale = tr.localScale;
		
		if (tr.parent != null) {
			parent = tr.parent.gameObject;
		}
	}
	
	function setParentIfExists(go : GameObject) {
		if (parent != null && go.transform != null) {
			go.transform.SetParent(parent.transform, true);
		}
	}
	
	function setToGameObject(go : GameObject) {
		go.transform.position = position;
		go.transform.rotation = rotation;
		go.transform.localScale = localScale;
		
		setParentIfExists(go);
	}
}