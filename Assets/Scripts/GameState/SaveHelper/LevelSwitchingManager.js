﻿#pragma strict

var gameKeyPrefab : GameObject;

function Start () {
	if (!PlayerPrefs.HasKey("level_needs_reload")) {
		Destroy(gameObject);
	}
}

function Update() {
	if (SaveHelper.getSceneGameKeys().length != 0 && GameObject.Find("GameState") != null && SaveHelper.getPlayer() != null) {
		setNeedsReload(false);
		new SaveHelper().load(gameKeyPrefab);
		Destroy(gameObject);
	}
}

public static function setNeedsReload(needsReload : boolean) {
	if (needsReload) {
		PlayerPrefs.SetInt("level_needs_reload", 1);
	} else {
		PlayerPrefs.DeleteKey("level_needs_reload");
	}
}