﻿#pragma strict

import System.Collections.Generic;
import UnityEngine.SceneManagement;

public class SaveHelper {
	
	var player_position : Vector3;
	var player_rotation : Quaternion;
	var save_point : Vector3;

	var player_health_curr : int;
	var player_health_max : int;
	
	var keys_held : int;
	var player_has_weapon: boolean;
	var lighting_setup: String;
	var level: String;
	var keys_in_scene : int;

	var torchIsLit : boolean;

	var completedBattles : List.<String>;
	
	var gameKeys : List.<TransformSpec>;
	
	var lv1BullseyeBridge : TransformSpec;
	var L1BlockPush : TransformSpec;
	
	function save() {
		loadWorld();
		PlayerPrefs.SetString("game_state", JsonUtility.ToJson(this, false));
		Debug.Log("Saved game state: " + JsonUtility.ToJson(this, true));
	}
	
	static function canLoad() : boolean {
		return PlayerPrefs.HasKey("game_state");
	}
	
	static function getSceneGameKeys() : GameObject[] {
		return GameObject.FindGameObjectsWithTag("GameKey");
	}
	
	static function getPlayer() : GameObject {
		return GameObject.FindWithTag("Player");
	}
	
	private static function getGameStateManager() : GameStateManager {
		return GameObject.Find("GameState").GetComponent.<GameStateManager>();
	}
	
	private function loadWorld() {
		var player : GameObject = getPlayer();
		var gsm : GameStateManager = getGameStateManager();
		var player_health : HealthHaver = player.GetComponent.<HealthHaver>();
		
		player_position = player.transform.position;
		player_rotation = player.transform.rotation;
		save_point = gsm.savePoint;
		completedBattles = gsm.completedBattles;
		
		player_health_curr = player_health.curr_health;
		player_health_max = player_health.max_health;
		
		keys_held = gsm.keys_held;
		keys_in_scene = gsm.keys_in_scene;
		player_has_weapon = gsm.has_weapon;
		
		lighting_setup = gsm.lightingSetup.gameObject.name;
		level = SceneManager.GetActiveScene().name;
		
		gameKeys = new List.<TransformSpec>();

		var playerTorch : Light = player.GetComponentInChildren.<Light>();
		torchIsLit = null != playerTorch && playerTorch.gameObject.activeInHierarchy;
		
		for (var keyGO : GameObject in getSceneGameKeys()) {
			gameKeys.Add(new TransformSpec(keyGO.transform));
		}
		
		var tmpLv1BullseyeBridge = GameObject.Find("BullseyeBridge");
		if (null != tmpLv1BullseyeBridge) {
			lv1BullseyeBridge = new TransformSpec(tmpLv1BullseyeBridge.transform);
		}
		
		var tmpL1BlockPush = GameObject.Find("L1BlockPush");
		if (null != tmpL1BlockPush) {
			L1BlockPush = new TransformSpec(tmpL1BlockPush.transform);
		}
	}
	
	private static function getAllGameObjects(scene : Scene) : List.<GameObject> {
		
	}
	
	static function load(gameKeyPrefab : GameObject) : boolean {
		if (canLoad()) {
			var h : SaveHelper = JsonUtility.FromJson(PlayerPrefs.GetString("game_state"), SaveHelper);
			Debug.Log("Loaded game state: " + JsonUtility.ToJson(h, true));
			
			if (h.level != SceneManager.GetActiveScene().name) {
				LevelSwitchingManager.setNeedsReload(true);
				SceneManager.LoadScene(h.level, LoadSceneMode.Single);
			} else {
				var player : GameObject = getPlayer();
				var player_health : HealthHaver = player.GetComponent.<HealthHaver>();
				var gsm : GameStateManager = getGameStateManager();
				
				player.transform.position = h.player_position;
				player.transform.rotation = h.player_rotation;
				gsm.savePoint = h.save_point;
				gsm.completedBattles = h.completedBattles;

				if (null != h.torchIsLit) {
				   var playerTorch : Light = player.GetComponentInChildren.<Light>();
				   if (null != playerTorch) {
				      playerTorch.gameObject.SetActive(h.torchIsLit);
				   }
				}

				if (h.completedBattles.Count > 0) {
					for (var battle : GameObject in GameObject.FindGameObjectsWithTag("BattleManager")) {
						var bm : BattleManager = battle.GetComponent.<BattleManager>();

						if (h.completedBattles.Contains(bm.battleID)) {
							Debug.Log(String.Format("Battle {0} restored from save", bm.battleID));
							bm.triggerEnd(true);
						}
					}
				}
				
				player_health.curr_health = h.player_health_curr;
				player_health.max_health = h.player_health_max;
				
				gsm.keys_held = h.keys_held;
				gsm.setHasWeapon(h.player_has_weapon);
				
				gsm.setLighing(h.lighting_setup);
				
				for (var keyGO : GameObject in getSceneGameKeys()) {
					GameObject.Destroy(keyGO);
				}
				
				for (var ts : TransformSpec in h.gameKeys) {
					var keyObject : GameObject = GameObject.Instantiate(gameKeyPrefab, ts.position, ts.rotation);
					ts.setParentIfExists(keyObject);
				}
				gsm.keys_in_scene = h.keys_in_scene;
				
				var tmpLv1BullseyeBridge = GameObject.Find("BullseyeBridge");
				if (null != tmpLv1BullseyeBridge && null != h.lv1BullseyeBridge) {
					h.lv1BullseyeBridge.setToGameObject(tmpLv1BullseyeBridge);
				}
				
				var tmpL1BlockPush = GameObject.Find("L1BlockPush");
				if (null != tmpL1BlockPush && null != h.L1BlockPush) {
					h.L1BlockPush.setToGameObject(tmpL1BlockPush);
				}
			}
		}
		
		return false;
	}
}