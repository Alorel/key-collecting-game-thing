﻿#pragma strict

var fps_text : UnityEngine.UI.Text;
var fpsMeasurePeriod : float = 0.5;

private var m_FpsAccumulator : int  = 0;
private var m_FpsNextPeriod : float = 0;
private var m_CurrentFps : int;

private var FORMAT : String = "FPS: {0}";

function Start () {
	m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
}

function Update () {
	m_FpsAccumulator++;
	
	if (Time.realtimeSinceStartup > m_FpsNextPeriod) {
		var fpsFloat : float = m_FpsAccumulator/fpsMeasurePeriod;
		
		m_CurrentFps = 0 + fpsFloat;
		m_FpsAccumulator = 0;
		m_FpsNextPeriod += fpsMeasurePeriod;
		fps_text.text = String.Format(FORMAT, m_CurrentFps);
	}
}