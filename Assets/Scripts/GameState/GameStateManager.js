﻿#pragma strict

import UnityEngine.SceneManagement;
import System.Collections.Generic;

var keys_held : int = 0;
private var _keys_in_scene : int;
private var _health : HealthHaver;

var has_weapon : boolean = false;

var hp_text : UnityEngine.UI.Text;
var key_text : UnityEngine.UI.Text;
var targetting_text : UnityEngine.UI.Text;
var toastPrefab : GameObject;

@Tooltip("Number of additional GameKeys that will eventually be generated")
var extraGameKeys : int = 0;

var saveButton : KeyCode = KeyCode.F1;
var loadButton : KeyCode = KeyCode.F2;

private var HP_TEXT_FORMAT : String = "{0}/{1}";
private var KEY_TEXT_FORMAT : String = "{0}/{1}";
private var TARGETTING_TEXT_FORMAT : String = "{0}";
private var TARGETTING_NPC_TEXT_FORMAT : String = "{0} [{1}/{2}]";

var raycastHit : RaycastHit;
var raycastDistance : float = Mathf.Infinity;

private var player : GameObject;

var savePoint : Vector3;

private var lightingController : LightingController;
var lightingSetup : LightingSetup;
var gameKeyPrefab : GameObject;
private var scene : String;

var completedBattles : List.<String> = new List.<String>();

var defaultSceneLighting: String;

function load() {
	if (SaveHelper.canLoad()) {
		SaveHelper.load(gameKeyPrefab);
		toast("Game loaded!");
	} else {
		toast("No saved game data");
	}
}

public function addCompletedBattle(battleID : String) {
	if (!completedBattles.Contains(battleID)) {
		completedBattles.Add(battleID);
	}
}

public function get keys_in_scene () : int {
	return _keys_in_scene;
}

function set keys_in_scene (value : int) {
	_keys_in_scene = value;
}

function save() {
	var halp : SaveHelper = new SaveHelper();
	halp.save();
	toast("Game saved");
}

function toast(msg: String) {
	Instantiate(toastPrefab).GetComponent.<MessageSetter>().setMessage(msg);
}

function setSavePoint(p : Vector3) {
	savePoint = p;
}

public function get health() : HealthHaver {
	return _health;
}

function Update () {
	hp_text.text = String.Format(HP_TEXT_FORMAT, health.curr_health, health.max_health);
	key_text.text = String.Format(KEY_TEXT_FORMAT, keys_held, keys_in_scene);

	if (Cursor.lockState != CursorLockMode.Locked) {
		Cursor.lockState = CursorLockMode.Locked;
	}
	
	if (Input.GetKeyDown(saveButton)) {
		if (!BattleManager.battleIsActive()) {
			save();
		} else {
			toast("You can't save the game whilst in battle, silly goose!");
		}
	} else if (Input.GetKeyDown(loadButton)) {
		load();
	}
	
	updateRaycast();
}

function Start() {
	countKeysInScene();
	keys_in_scene += extraGameKeys;
	lightingController = GameObject.Find("LightingController").GetComponent.<LightingController>();
	player = GameObject.FindWithTag("Player");
	_health = player.GetComponent.<HealthHaver>();
	scene = SceneManager.GetActiveScene().name;
	
	savePoint = player.transform.position;
	if (null != defaultSceneLighting) {
		setLighing(defaultSceneLighting);
	}
}

function countKeysInScene() : int {
	keys_in_scene = GameObject.FindGameObjectsWithTag("GameKey").length;
	return keys_in_scene;
}

function setLighing(setup : String) {
	if (null != setup) {
		lightingSetup = lightingController.fetch(setup);
		if (null != lightingSetup) {
			lightingSetup.changeLiveSettings();
			Debug.Log("Lighting set to " + lightingSetup.gameObject.name);
		}
	}
}

function setHasWeapon(setting : boolean) {
	Debug.Log("Player now has weapon: "+setting);
	has_weapon = setting;
	player.GetComponent.<Shooter>().can_shoot = setting;
}

function getHasWeapon() : boolean {
	return has_weapon;
}

function getRaycastHit() : RaycastHit {
	return raycastHit;
}

function addKey() {
	keys_held++;
}

function removeKeys(num: int) {
	keys_held -= num;
}


private function updateRaycast() : void {
	var ray: Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	if (Physics.Raycast(ray, raycastHit, raycastDistance)) {
		var go : GameObject = raycastHit.transform.gameObject;
		var hh : HealthHaver = go.GetComponent.<HealthHaver>();

		if (hh != null) {
			targetting_text.text = String.Format(TARGETTING_NPC_TEXT_FORMAT, go.name, hh.curr_health, hh.max_health);
		} else {
			targetting_text.text = String.Format(TARGETTING_TEXT_FORMAT, go.name);
		}
	} else {
		targetting_text.text = String.Format(TARGETTING_TEXT_FORMAT, "nothing");
	}
}