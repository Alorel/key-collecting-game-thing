﻿#pragma strict

import System.Collections.Generic;

private var available : Dictionary.<String, LightingSetup> = new Dictionary.<String, LightingSetup>();

var sceneLight : Light;

function Awake () {
	for (var s : LightingSetup in gameObject.GetComponentsInChildren(LightingSetup)) {
		available.Add(s.gameObject.name, s);
	}
}

function loadFromPrefs() : boolean {
	if (PlayerPrefs.HasKey("lighting_setup")) {
		fetch(PlayerPrefs.GetString("lighting_setup")).changeLiveSettings();
		
		return true;
	}
		
	return false;
}

function fetch(name : String) : LightingSetup {
	var oot : LightingSetup;
	available.TryGetValue(name, oot);
	
	return oot;
}