#pragma strict

var skybox : Material;
var ambientMode : Rendering.AmbientMode;
var sceneLightEnabled : boolean;

private var sceneLight : Light;

function Awake() {
	sceneLight = gameObject.GetComponentInParent.<LightingController>().sceneLight;
}

function changeLiveSettings() {
	RenderSettings.skybox = skybox;
	RenderSettings.ambientMode = ambientMode;
	
	if (null != sceneLight) {
		sceneLight.enabled = sceneLightEnabled;
	}
}