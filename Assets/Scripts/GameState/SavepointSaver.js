﻿#pragma strict

private var gameState : GameStateManager;

function Start() {
	gameState = GameObject.Find("GameState").GetComponent.<GameStateManager>();
}

function OnTriggerEnter (collider : Collider) {
    if (collider.gameObject.tag == "Player") {
		gameState.savePoint = collider.gameObject.transform.position;
    }
}