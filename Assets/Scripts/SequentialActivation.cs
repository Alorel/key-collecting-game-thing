﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SequentialActivation : MonoBehaviour {

	public bool activatedViaTrigger = true;
	public bool autoActivate = false;

	[Header("Execution")]
	public GameObject[] objects;

	[Range(1f, 600f)]
	public float duration;

	[Header("ProgressListeners")]
	public List<GameObject> activationListeners = new List<GameObject>();
	public string objectActivationEvent = "onObjectActivation";
	public string sequenceFinishEvent = "onActivationSequenceFinish";

	private bool started = false;
	private float timeElapsed = 0f;
	private int nextActivationIndex = 0;
	private float nextActivationTime;
	private float timeIncrements;

	// Use this for initialization
	void Start () {
		activationListeners.Add (gameObject);
		timeIncrements = duration / (float)objects.Length;
		nextActivationTime = timeIncrements;

		if (autoActivate) {
			beginActivation ();
		}
	}

	public void beginActivation() {
		if (!started) {
			started = true;
		}
	}

	void OnTriggerEnter(Collider col) {
		if (activatedViaTrigger) {
			beginActivation ();
		}
	}

	void FixedUpdate () {
		if (started) {
			if (nextActivationIndex >= objects.Length) {
				foreach (GameObject o in activationListeners) {
					o.BroadcastMessage (sequenceFinishEvent, gameObject, SendMessageOptions.DontRequireReceiver);
				}
				Destroy (this); //done
			} else {
				timeElapsed += Time.fixedDeltaTime;

				if (nextActivationTime <= timeElapsed) {
					objects[nextActivationIndex].SetActive (true);

					foreach (GameObject o in activationListeners) {
						o.BroadcastMessage (objectActivationEvent, objects[nextActivationIndex], SendMessageOptions.DontRequireReceiver);
					}

					nextActivationIndex++;
					nextActivationTime += timeIncrements;
				}
			}
		}
	}
}
