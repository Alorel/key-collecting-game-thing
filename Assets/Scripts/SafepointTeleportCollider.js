﻿#pragma strict

private var gameState : GameStateManager;

function Start() {
	gameState = GameObject.Find("GameState").GetComponent.<GameStateManager>();
}

private function handle(go : GameObject) {
	if (go.tag == "Player") {
		go.transform.position = gameState.savePoint;
	}
}

function OnCollisionEnter (collision : Collision) {
	handle(collision.gameObject);
}

function OnTriggerEnter (collision : Collider) {
	handle(collision.gameObject);
}