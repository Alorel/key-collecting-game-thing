﻿#pragma strict

import System.Collections.Generic;

public class BattleManager extends MonoBehaviour {

	enum BattleEndTrigger {HP, Destroy};

	var backgroundMusicManagerGameObject : GameObject;

	var battleID : String;

	public var toastPrefab : GameObject;

	private var bgMusicManager : BackgroundMusicManager;

	@Header("GameObject management")
	var initObjectsOnBattleStart : GameObject[] = [];

	@Tooltip("This should NOT contain the battle manager gameobject")
	var destroyObjectsOnBattleEnd : GameObject[] = [];

	@Tooltip("Destroy the battle manager when the battle ends?")
	public var destroyThisOnEnd : boolean = true;

	@Header("Flow management")
	@Tooltip("What sets the battle to an ended state. HP means that all enemies must be dead whilst Destroy means that all enemy GameObjects must become null")
	var battleEndTrigger : BattleEndTrigger = BattleEndTrigger.Destroy;

	var enemies : List.<GameObject> = new List.<GameObject>();

	@Tooltip("Event that will get broadcasted when the battle ends")
	var onBattleEnd : String = "onBattleEnd";

	private var battleHasBeenStarted : boolean = false;
	private var battleHasEnded : boolean = false;

	private static var _battleIsActive : boolean = false;

	function Start () {
		bgMusicManager = (null == backgroundMusicManagerGameObject ? GameObject.FindWithTag("BGMusicManager") : backgroundMusicManagerGameObject).GetComponent.<BackgroundMusicManager>();
		if (null == battleID) {
			battleID = gameObject.name;
		}
	}

	public static function battleIsActive() : boolean {
		return _battleIsActive;
	}

	function addEnemy(enemy : GameObject) {
		enemies.Add(enemy);
	}

	protected function get musicMethod() : String {
		return "playBattle";
	}

	function toast(msg : String) {
		Instantiate(toastPrefab).GetComponentInChildren.<Text>().text = msg;
	}

	function startBattle() : boolean {
		if (!battleHasBeenStarted) {
			BroadcastMessage("onBattleStart", battleID, SendMessageOptions.DontRequireReceiver);
			Debug.Log("Starting battle " + battleID);
			bgMusicManager.SendMessage(musicMethod, null);
			for (var o : GameObject in initObjectsOnBattleStart) {
				o.SetActive(true);
			}
			_battleIsActive = true;
			battleHasBeenStarted = true;

			return true;
		}

		return false;
	}

	private function enemyMeetsEndCondition(enemy : GameObject) : boolean {
		if (enemy != null) {
			return battleEndTrigger == BattleEndTrigger.Destroy ? false : enemy.GetComponent.<HealthHaver>().dead;
		}
		return true;
	}

	private function get endConditionIsMet() : boolean {
		if (enemies.Count > 0) {
			for (var enemy : GameObject in enemies) {
				if (!enemyMeetsEndCondition(enemy)) {
					return false;
				}
			}
		}

		return true;
	}

	function triggerEnd(fromSave : boolean) {
		Debug.Log("Ending battle " + battleID);
		battleHasEnded = true;
		_battleIsActive = false;
		GameObject.Find("GameState").GetComponent.<GameStateManager>().addCompletedBattle(battleID);
		bgMusicManager.playBackground();
		gameObject.BroadcastMessage(onBattleEnd, fromSave, SendMessageOptions.DontRequireReceiver);

		for (var go : GameObject in destroyObjectsOnBattleEnd) {
			Destroy(go);
		}

		if (destroyThisOnEnd) {
			Destroy(gameObject);
		}
	}

	protected function get continueUpdates() : boolean {
		return battleHasBeenStarted && !battleHasEnded;
	}

	function Update () {
		if (continueUpdates) {
			if (endConditionIsMet) {
				triggerEnd(false);
			}
		}
	}
}