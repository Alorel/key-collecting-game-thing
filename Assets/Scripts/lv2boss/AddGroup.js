﻿#pragma strict

public class AddGroup {

	private var adds : List.<GameObject>;

	public function AddGroup(listOfAdds : List.<GameObject>) {
		adds = listOfAdds;
	}

	public function get cleared() : boolean {
		for (var add : GameObject in adds) {
			if (add != null && !add.GetComponent.<HealthHaver>().dead) {
				return false;
			}
		}
		return true;
	}
}