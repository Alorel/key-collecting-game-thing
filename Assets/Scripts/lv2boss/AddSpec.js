﻿#pragma strict

import System.Collections.Generic;

public class AddSpec {

	private var mobsAndCoords : Dictionary.<GameObject, Vector3[]> = new Dictionary.<GameObject, Vector3[]>();
	private var spawned : boolean = false;

	public function AddSpec() {

	}

	public function AddSpec(prefab : GameObject, coords : Vector3[]) {
		add(prefab, coords);
	}

	public function add(prefab : GameObject, coords : Vector3[]) : AddSpec {
		mobsAndCoords.Add(prefab, coords);
		return this;
	}

	public function spawn() : AddGroup {
		if (!spawned) {
			spawned = true;
			var l : List.<GameObject> = new List.<GameObject>();

			for (var pair : KeyValuePair.<GameObject, Vector3[]> in mobsAndCoords) {
				for (var pos : Vector3 in pair.Value) {
					Debug.Log(String.Format("Spawning {0} at {1}", pair.Key.name, pos));
					l.Add(GameObject.Instantiate(pair.Key, pos, Quaternion.identity) as GameObject);
				}
			}

			return new AddGroup(l);
		}

		return null;
	}
}