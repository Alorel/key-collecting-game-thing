﻿using UnityEngine;
using System.Collections;

[RequireComponent((typeof(AudioSource)))]
public class FenceUp : MonoBehaviour {

	private AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}

	private IEnumerator startCleanup() {
		this.audio.Play ();
		yield return new WaitForSeconds (this.audio.clip.length);
		this.audio.enabled = false;
	}

	void posAnimationEnd() {
		StartCoroutine (startCleanup ());
	}
}
