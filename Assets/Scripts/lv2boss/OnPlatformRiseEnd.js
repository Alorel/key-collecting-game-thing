﻿#pragma strict

@script RequireComponent(HealthHaver)
@script RequireComponent(Shooter)

private function platformRiseEnded(platform : GameObject) {
	gameObject.GetComponent.<HealthHaver>().immuneOverride = false;
	gameObject.GetComponent.<Shooter>().can_shoot = true;
	gameObject.transform.parent.parent.gameObject.GetComponent.<BattleManager>().startBattle();
	Destroy(this);
}