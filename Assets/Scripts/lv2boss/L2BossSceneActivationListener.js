﻿#pragma strict

var bossPlatform : GameObject;

private var _platforms : boolean = false;
private var _torches : boolean = false;

function torchesActivated() {
	_torches = true;
	if (_platforms) {
		doActivation();
	}
}

private function doActivation() {
	bossPlatform.SetActive(true);
	Destroy(this);
}

function platformsActivated() {
	_platforms = true;
	if (_torches) {
		doActivation();
	}
}