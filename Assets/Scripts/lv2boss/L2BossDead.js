﻿#pragma strict

var fence : GameObject;
var ringOfFireStart : GameObject;
var ringOfFireEnd : GameObject;

private function corou() : IEnumerator {
	var player : GameObject = GameObject.FindWithTag("Player");
	Destroy(ringOfFireStart);
	ringOfFireEnd.SetActive(true);

	yield WaitForSeconds(6.861f);
	player.SendMessage("enableLighting", true);
	fence.SendMessage("startAnimation");
	Destroy(gameObject);
}

function onDeath() {
	StartCoroutine(corou());
}