﻿#pragma strict

public class Lv2BossBattleManager extends BossBattleManager {

	@Header("Adds")
	public var skeletonPrefab : GameObject;
	public var crawlerPrefab : GameObject;

	@Header("SFX")
	public var sfxImmortal : AudioClip;

	@Header("Difficulty")
	public var shootingDelayDecrease : float = 0.9f;

	private static var L1 : Vector3 = new Vector3(340.1798, 107.11, 165.2583);
	private static var L2 : Vector3 = new Vector3(340.27, 107.11, 168.77);
	private static var L3 : Vector3 = new Vector3(341.654, 107.11, 172.276);
	private static var L4 : Vector3 = new Vector3(344.42, 107.11, 174.5);
	
	private static var R1 : Vector3 = new Vector3(372.62, 107.11, 155.73);
	private static var R2 : Vector3 = new Vector3(376.81, 107.11, 162.97);
	private static var R3 : Vector3 = new Vector3(375.75, 107.11, 169.595);
	private static var R4 : Vector3 = new Vector3(369.412, 107.11, 174.945);

	private var audioSource : AudioSource;
	private var shooter : Shooter;

	function Start() {
		super.Start();
		shooter = boss.GetComponent.<Shooter>();
		audioSource = boss.GetComponent.<AudioSource>();
		initAdds();
	}

	private function addsSpawned(hp : int) {
		audioSource.clip = sfxImmortal;
		audioSource.Play();
		bossHealth.immuneOverride = true;
		toast("Fool! I am immortal!");
	}

	private function addsDefeated(hp : int) {
		shooter.shootFrequency *= shootingDelayDecrease;
		bossHealth.immuneOverride = false;
		toast("The boss is vulnerable again!");
	}

	private function initAdds() {
		var g90: AddSpec;
		var g80: AddSpec;
		var g70: AddSpec;
		var g60: AddSpec;
		var g50: AddSpec;
		var g40: AddSpec;
		var g30: AddSpec;
		var g20: AddSpec;
		var g10: AddSpec;

		g90 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L1,
					R4
				]
			);

		g80 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L4,
					R1,
					R3
				]
			);

		g70 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L1,
					R4
				]
			).add(
				crawlerPrefab,
				[
					R1
				]
			);

		g60 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L1,
					R4
				]
			).add(
				crawlerPrefab,
				[
					L4,
					R1
				]
			);

		g50 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L2,
					L3,
					L4
				]
			).add(
				crawlerPrefab,
				[
					R2,
					R3,
					R4
				]
			);

		g40 = new AddSpec()
			.add(
				crawlerPrefab,
				[
					L1,
					L2,
					L3,
					L4,
					R1,
					R4
				]
			);

		g30 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					R1,
					R2,
					R3,
					R4,
					L1,
					L4
				]
			);

		g20 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L1,
					L3,
					L4,
					R4,
					R2,
					R1
				]
			).add(
				crawlerPrefab,
				[
					L1,
					R4,
					R1,
					L4
				]
			);

		g10 = new AddSpec()
			.add(
				skeletonPrefab,
				[
					L1,
					L2,
					L3,
					L4,
					R4,
					R3,
					R2,
					R1
				]
			).add(
				crawlerPrefab,
				[
					L1,
					L2,
					L3,
					L4,
					R1,
					R2,
					R3,
					R4
				]
			);

		allAdds.Add(90, g90);
		allAdds.Add(80, g80);
		allAdds.Add(70, g70);
		allAdds.Add(60, g60);
		allAdds.Add(50, g50);
		allAdds.Add(40, g40);
		allAdds.Add(30, g30);
		allAdds.Add(20, g20);
		allAdds.Add(10, g10);
	}
}