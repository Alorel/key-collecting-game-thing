﻿#pragma strict

import System.Collections.Generic;

public class BossBattleManager extends BattleManager {

	protected var bossHealth : HealthHaver;
	protected var bossShooter : Shooter;

	protected var allAdds : Dictionary.<int, AddSpec> = new Dictionary.<int, AddSpec>();
	protected var currAdds : AddGroup = null;
	public var onAddSpawnEvent : String = "addsSpawned";
	public var onAddDefeatEvent : String = "addsDefeated";

	function Start() {
		super.Start();
		bossHealth = boss.GetComponent.<HealthHaver>();
		bossShooter = boss.GetComponent.<Shooter>();
	}

	protected function get boss() : GameObject {
		return enemies[0];
	}

	protected function get currHealth() : int {
		return bossHealth.curr_health;
	}

	protected function get musicMethod() : String {
		return "playBoss";
	}

	function Update () {
		super.Update();
		if (continueUpdates) {
			if (currAdds == null) {
				var addSpec : AddSpec;

				allAdds.TryGetValue(currHealth, addSpec);

				if (null != addSpec) {
					Debug.Log("Detected add spec at " + currHealth + " HP");
					currAdds = addSpec.spawn();
					allAdds.Remove(currHealth);
					BroadcastMessage(onAddSpawnEvent, currHealth, SendMessageOptions.DontRequireReceiver);
				}
			} else {
				if (currAdds.cleared) {
					Debug.Log(currHealth + " HP adds defeated");
					currAdds = null;
					BroadcastMessage(onAddDefeatEvent, currHealth, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}
}