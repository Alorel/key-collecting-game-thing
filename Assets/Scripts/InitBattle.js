﻿#pragma strict

@Tooltip("Game object containing the battle manager")
var battleManagerGO : GameObject;

private function initBattle(go : GameObject) {
	if (go.tag == "Player") {
		battleManagerGO.GetComponent.<BattleManager>().startBattle();
		Destroy(this);
	}
}

function OnCollisionEnter (col : Collision) {
	initBattle(col.gameObject);
}

function OnTriggerEnter(col : Collider) {
	initBattle(col.gameObject);
}