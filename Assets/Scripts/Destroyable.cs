﻿using UnityEngine;
using System.Collections;

public class Destroyable : MonoBehaviour {
	
	public int numParentsToDestroy = 0;

	public void DestroyMe() { 
		if (numParentsToDestroy < 1) {
			Destroy(gameObject); 
		} else {
			int i = 0;
			Transform previous;
			Transform curr = transform;
			
			do {
				previous = curr;
				curr = curr.transform.parent;
				i++;
			} while (curr != null && i < numParentsToDestroy);
			
			if (null != curr) {
				Destroy(curr.gameObject);
			} else {
				Destroy(previous.gameObject);
			}
		}
	}
}