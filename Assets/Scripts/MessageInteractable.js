﻿#pragma strict

var message : String;

var toastPrefab : GameObject;

function onInteraction(rh : RaycastHit) {
	Instantiate(toastPrefab, transform.position, Quaternion.identity).GetComponent.<MessageSetter>().setMessage(message);
}