﻿#pragma strict

public class JSPositionAnimator extends MonoBehaviour {
	public var targetPosition: Vector3 ;

	public var updateCycle : UpdateCycleJS  = UpdateCycleJS.FixedUpdate;

	public var speed : float = 3;

	public var autostart : boolean = true;

	private var started : boolean = false;

	@Header("End listeners")
	public var endListeners : GameObject[] = [];
	public var endEvent : String = "posAnimationEnd";

	// Use this for initialization
	function Start () {
		if (autostart) {
			startAnimation ();
		}
	}

	function startAnimation() {
		if (targetPosition != null) {
			started = true;
		}
	}

	function Update() {
		if (updateCycle == UpdateCycleJS.Update) {
			doUpdate (Time.deltaTime);
		}
	}

	private function doUpdate( diff: float) {
		if (started) {
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, diff * speed);

			if (transform.position == targetPosition) {
				for (var o : GameObject in endListeners) {
					o.BroadcastMessage (endEvent, gameObject, SendMessageOptions.DontRequireReceiver);
				}
				Destroy (this);
			}
		}
	}

	// Update is called once per frame
	function FixedUpdate () {
		if (updateCycle == UpdateCycleJS.FixedUpdate) {
			doUpdate (Time.fixedDeltaTime);
		}
	}
}