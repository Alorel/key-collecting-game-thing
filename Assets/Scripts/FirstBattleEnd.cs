﻿using UnityEngine;
using System.Collections;

public class FirstBattleEnd : MonoBehaviour {

	public GameObject physxGameKeyPrefab;

	public GameObject floater;

	public Vector3[] spawnLocs;

	void onBattleEnd(bool fromSave) {
		if (!fromSave) {
			GameObject.Find ("GameState").SendMessage("setSavePoint", new Vector3 (256.78f, 100.54f, 125.66f));

			foreach (Vector3 vector in spawnLocs) {
				Instantiate (physxGameKeyPrefab, vector, Quaternion.identity);
			}
		}
		floater.SetActive (true);
	}
}
