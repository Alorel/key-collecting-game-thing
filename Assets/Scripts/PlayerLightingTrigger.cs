﻿using UnityEngine;
using System.Collections;

public class PlayerLightingTrigger : MonoBehaviour {

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Player") {
			GameObject.FindWithTag ("Player").SendMessage ("disableLighting");
			Destroy (this);
		}
	}
}
