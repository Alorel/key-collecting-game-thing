﻿using UnityEngine;
using System.Collections;

public class TriggerActivator : MonoBehaviour {

	public GameObject[] objectsToActivate;

	void Awake() {
		foreach (GameObject o in objectsToActivate) {
			o.SetActive (false);
		}
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Player") {
			foreach (GameObject o in objectsToActivate) {
				o.SetActive (true);
			}
			Destroy (this);
		}
	}
}
