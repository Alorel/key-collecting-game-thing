﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof (RigidbodyFirstPersonController))]
public class MovementSounds : MonoBehaviour {
	
	public AudioClip[] footstepSounds;
	public AudioClip jumpSound;
	public AudioClip landSound;
	
	public float volume = 0.5f;
	
	public float playFootstepSoundsEvery = 0.5f;
	
	public float runFootstepFrequencyModifier = 1.5f;
	
	private int footstepIndex = 0;
	private static Vector3 NO_VELOCITY = new Vector3(0, 0, 0);
	private float timeSinceLastFootstep = 0f;
	private RigidbodyFirstPersonController fpc;
	
	private bool wasPreviouslyJumping;
	private bool wasPreviouslyGrounded;

	// Use this for initialization
	void Start () {
		gameObject.AddComponent<AudioSource>();
		fpc = gameObject.GetComponent<RigidbodyFirstPersonController>();
		
		wasPreviouslyGrounded = fpc.Grounded;
		wasPreviouslyJumping = fpc.Jumping;
	}
	
	private bool moving {
		get {
			return fpc.Velocity != NO_VELOCITY;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (fpc.Jumping && !wasPreviouslyJumping) {
			AudioSource.PlayClipAtPoint(jumpSound, transform.position, volume);
		} else if (fpc.Grounded && !wasPreviouslyGrounded) {
			AudioSource.PlayClipAtPoint(landSound, transform.position, volume);
		} else {
			timeSinceLastFootstep += Time.deltaTime;
			if (fpc.Grounded && moving && (timeSinceLastFootstep > playFootstepSoundsEvery || (fpc.Running && timeSinceLastFootstep > playFootstepSoundsEvery / runFootstepFrequencyModifier))) {
				timeSinceLastFootstep = 0;
				AudioSource.PlayClipAtPoint(footstepSounds[(footstepIndex++) % footstepSounds.Length], transform.position, volume);
			}
		}
		wasPreviouslyJumping = fpc.Jumping;
		wasPreviouslyGrounded = fpc.Grounded;
	}
}
