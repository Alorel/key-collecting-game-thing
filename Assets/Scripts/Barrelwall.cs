﻿using UnityEngine;
using System.Collections;

public class Barrelwall : MonoBehaviour {

	public GameObject barrelWall;

	private Rigidbody[] barrels;

	private bool done = false;

	// Use this for initialization
	void Awake () {
		barrels = barrelWall.GetComponentsInChildren<Rigidbody>();

		foreach (Rigidbody r in barrels) {
			r.isKinematic = true;
		}
	}
	
	void OnTriggerEnter(Collider c) {
		if (!done && c.gameObject.tag == "Player") {
			Debug.Log ("Barrel wall triggered");
			foreach (Rigidbody r in barrels) {
				r.isKinematic = false;
			}
			done = true;
		}
	}
}
