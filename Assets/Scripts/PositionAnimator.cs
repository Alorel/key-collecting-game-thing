﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Transform))]
public class PositionAnimator : MonoBehaviour {

	public Vector3 targetPosition;
	public bool isRelative = false;

	public UpdateCycle updateCycle = UpdateCycle.FixedUpdate;

	public float speed = 3;

	public bool autostart = true;

	private bool started = false;

	[Header("End listeners")]
	public GameObject[] endListeners = {};
	public string endEvent = "posAnimationEnd";

	// Use this for initialization
	void Start () {
		if (isRelative) {
			targetPosition = new Vector3 (
				transform.position.x + targetPosition.x,
				transform.position.y + targetPosition.y,
				transform.position.z + targetPosition.z
			);
		}

		if (autostart) {
			startAnimation ();
		}
	}

	public void startAnimation() {
		if (targetPosition != null) {
			started = true;
		}
	}

	void Update() {
		if (updateCycle == UpdateCycle.Update) {
			doUpdate (Time.deltaTime);
		}
	}

	private void doUpdate(float diff) {
		if (started) {
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, diff * speed);

			if (transform.position == targetPosition) {
				Debug.Log (gameObject.name + "pos animation ended.");
				foreach (GameObject o in endListeners) {
					Debug.Log (string.Format("Sending {0} to {1}", endEvent, o.name));
					o.BroadcastMessage (endEvent, gameObject, SendMessageOptions.DontRequireReceiver);
				}
				Destroy (this);
			}
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (updateCycle == UpdateCycle.FixedUpdate) {
			doUpdate (Time.fixedDeltaTime);
		}
	}
}
