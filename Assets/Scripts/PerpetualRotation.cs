﻿using UnityEngine;
using System.Collections;

public class PerpetualRotation : MonoBehaviour {

	public float xDegreesPerSecond = 0f;
	public float yDegressPerSecond = 0f;
	public float zDegreesPerSecond = 0f;
	
	// Update is called once per frame
	void Update () {
		float delta = Time.deltaTime;
		transform.Rotate (delta * xDegreesPerSecond, delta * yDegressPerSecond, delta * zDegreesPerSecond);
	}
}
