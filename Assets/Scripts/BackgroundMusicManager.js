﻿#pragma strict

private var audioSource : AudioSource;

public var normalBgClip : AudioClip;
public var battleClip : AudioClip;
public var bossClip : AudioClip;

@Tooltip("AudioSource volume")
@RangeAttribute(0.0f, 1.0f)
public var volume : float = 0.8f;

@Tooltip("AudioSource priority")
@RangeAttribute(0, 256)
public var priority : int = 256;

function Awake() {
	audioSource = gameObject.AddComponent.<AudioSource>();
	audioSource.playOnAwake = false;
	audioSource.loop = true;
	audioSource.priority = 256;
	audioSource.volume = 0.8f;
	audioSource.spatialBlend = 0;
}

function Start() {
	playBackground();
}

function play(clip : AudioClip) {
	audioSource.Stop();
	audioSource.clip = clip;
	audioSource.Play();
}

function playBackground() {
	play(normalBgClip);
}

function playBattle() {
	play(battleClip);
}

function playBoss() {
	play(bossClip);
}