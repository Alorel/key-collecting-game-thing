﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Toggler : MonoBehaviour {
	
	public KeyCode toggleKey;

	public GameObject target;

	public bool togglerEnabled = true;

	public GameObject toastPrefab;

	public string toastMessageIfDisabled = "That's not gonna work...";
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (toggleKey)) {
			if (togglerEnabled) {
					target.SetActive (!target.activeInHierarchy);
			} else {
				Instantiate(toastPrefab).GetComponentInChildren<Text>().text = toastMessageIfDisabled;
			}
		}
	}
}
