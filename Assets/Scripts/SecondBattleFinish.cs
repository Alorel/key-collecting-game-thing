﻿using UnityEngine;
using System.Collections;

public class SecondBattleFinish : MonoBehaviour {

	public GameObject platform;

	public GameObject lightGO;

	private Light lightComponent;

	private bool destroyingLight = false;

	void Start() {
		lightComponent = lightGO.GetComponent<Light> ();
	}

	void Update() {
		if (destroyingLight) {
			float nu = lightComponent.intensity - Time.deltaTime;

			if (nu > 0f) {
				lightComponent.intensity = nu;
			} else {
				Destroy (lightGO);
				Destroy (this);
			}
		}
	}

	void onBattleEnd(bool fromSave) {
		Debug.Log ("Triggered");
		platform.GetComponent<Animator> ().enabled = true;
		destroyingLight = true;
	}
}
