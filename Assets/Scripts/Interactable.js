﻿#pragma strict

public var  maxDistance : float = 1.36f;

public var methodName : String = "onInteraction";

var toastPrefab : GameObject;

private static var  gsm : GameStateManager;

private var originalShader : Shader;

var targettingShader : Shader;

function Awake() {
	gsm = GameObject.Find("GameState").GetComponent.<GameStateManager>();
	originalShader = GetComponent.<Renderer>().material.shader;
}

function Update () {
	if (gsm.raycastHit != null && !gsm.health.dead && gsm.raycastHit.transform != null && gsm.raycastHit.transform.gameObject == gameObject) {
		GetComponent.<Renderer>().material.shader = targettingShader;
		
		if (Input.GetMouseButtonDown(1)) {
			if (gsm.raycastHit.distance < maxDistance) {
				BroadcastMessage(methodName, gsm.raycastHit, SendMessageOptions.DontRequireReceiver);
			} else {
				Instantiate(toastPrefab, transform.position, Quaternion.identity).GetComponent.<MessageSetter>().setMessage("It's too far away!");
			}
		}
	} else {
		GetComponent.<Renderer>().material.shader = originalShader;
	}
}