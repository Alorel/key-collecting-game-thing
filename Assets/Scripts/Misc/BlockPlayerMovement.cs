﻿using UnityEngine;
using System.Collections;

public class BlockPlayerMovement : MonoBehaviour {
	
	private Vector3 lockCoords;
	
	private Transform player;

	// Use this for initialization
	void Awake () {
		player = GameObject.FindWithTag("Player").transform;
	}
	
	void OnEnable() {
		updateLockCoords();
	}
	
	public void updateLockCoords() {
		updateLockCoords(player.position);
	}
	
	public void updateLockCoords(Vector3 vector) {
		lockCoords = vector;
	}
	
	void Update () {
		player.position = lockCoords;
	}
}
