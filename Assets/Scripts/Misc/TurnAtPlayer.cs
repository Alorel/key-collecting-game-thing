﻿using UnityEngine;
using System.Collections;

public class TurnAtPlayer : MonoBehaviour {

	private Transform player;

	public float turnDistance = 50;
	public Vector3 forceAdditionalRotation = new Vector3(0, 0, 0);

	public float distance {
		get {
			return Vector3.Distance(gameObject.transform.position, player.position);
		}
	}

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (distance <= turnDistance) {
			gameObject.transform.LookAt (player);

			gameObject.transform.Rotate (forceAdditionalRotation.x, forceAdditionalRotation.y, forceAdditionalRotation.z);
		}
	}
}
