﻿using UnityEngine;
using System.Collections;

public class MoveTowardsPlayer : MonoBehaviour {

	private Transform player;
	public float speed = 2.75f;
	public float stopAtDistance = 0.8f;

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 nu = Vector3.MoveTowards (transform.position, player.position, Time.fixedDeltaTime * speed);

		if (Vector3.Distance (nu, player.position) >= stopAtDistance) {
			transform.position = nu;
		} else {
			SendMessage ("targetReachedPlayer", gameObject, SendMessageOptions.DontRequireReceiver);
		}
	}
}
