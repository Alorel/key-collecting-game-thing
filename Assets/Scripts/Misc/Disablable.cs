﻿using UnityEngine;
using System.Collections;

public class Disablable : MonoBehaviour {
	
	public KeyCode triggerKey = KeyCode.Escape;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(triggerKey)) {
			enabled = false;
		}
	}
}
