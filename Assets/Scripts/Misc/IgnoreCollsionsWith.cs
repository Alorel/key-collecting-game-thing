﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class IgnoreCollsionsWith : MonoBehaviour {

	public GameObject[] targets;

	// Use this for initialization
	void Start () {
		Collider dis = gameObject.GetComponent<Collider> ();
		foreach (GameObject go in targets) {
			Collider dat = go.GetComponent<Collider> ();
			if (null != dat) {
				Physics.IgnoreCollision (dis, dat);
			}
		}
		Destroy (this);
	}
}
