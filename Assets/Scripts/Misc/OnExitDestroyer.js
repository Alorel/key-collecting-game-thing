﻿#pragma strict

var objectToDestroy : GameObject;

function OnTriggerExit(collider : Collider) {
	if ( collider.gameObject.tag == "Player") {
		Destroy(objectToDestroy ? objectToDestroy : gameObject);
	}
}