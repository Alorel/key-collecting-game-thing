﻿using UnityEngine;
using System.Collections;

public class Inactivatable : MonoBehaviour {

	public KeyCode triggerKey = KeyCode.Escape;
	
	public GameObject objectToDeactivate;
		
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(triggerKey)) {
			(objectToDeactivate != null ? objectToDeactivate : gameObject).SetActive(false);
		}
	}
}
