﻿using UnityEngine;
using System.Collections;

public class DestroyDevItems : MonoBehaviour {

	void Start () {
		foreach (GameObject go in GameObject.FindGameObjectsWithTag ("dev")) {
			Destroy(go);
		}
		Destroy (gameObject);
	}
}
