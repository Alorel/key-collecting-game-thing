﻿#pragma strict

@script RequireComponent(HealthHaver)
public class MeteorShooter extends Shooter {

	public var shootDistanceThreshold : float = 30f;
	public var turnDistanceThreshold : float = 30f;

	public var spawnDistance : float = 3f;

	@Header("Projectile motion")
	public var yRise : float = 15f;
	public var riseSpeed : float = 22.5f;
	public var projectileFireSpeed : float = 10f;

	private var playerHealth : HealthHaver;

	function Start() {
		super.Start();
		playerHealth = player.GetComponent.<HealthHaver>();
	}

	protected function endRise(meteor : GameObject) {
		meteor.GetComponent.<Rigidbody>().isKinematic = false;
		var anim : JSPositionAnimator = meteor.AddComponent.<JSPositionAnimator> ();
		anim.autostart = false;
		anim.speed = projectileFireSpeed;
		anim.targetPosition = player.transform.position;
		anim.startAnimation ();
	}

	protected function get distance() : float {
		return Vector3.Distance(player.transform.position, gameObject.transform.position);
	}

	protected function get shouldTurn() : boolean {
		return !dead && distance <= turnDistanceThreshold;
	}

	private function get riseTime() : float {
		return yRise / riseSpeed;
	}

	protected function ignoreCollisionWithAdds(go : GameObject) {
		var goC : Collider = go.GetComponent.<Collider>();

		for (var goA in GameObject.FindGameObjectsWithTag("Add")) {
			var c : Collider = goA.GetComponent.<Collider>();

			if (null != c) {
				Physics.IgnoreCollision(c, goC);
			}
		}
	}

	protected function instantiateProjectile() : GameObject {
		var spawnPosition : Vector3 = spawnLoc;

		var go : GameObject =  Instantiate(
			projectilePrefab, 
			spawnPosition, 
			Quaternion.identity
		) as GameObject;
		addCollisionIgnore(go);
		ignoreCollisionWithAdds(go);

		var anim : JSPositionAnimator = go.AddComponent.<JSPositionAnimator> ();

		anim.speed = riseSpeed;
		anim.targetPosition = new Vector3 (spawnPosition.x, spawnPosition.y + yRise, spawnPosition.z);
		anim.startAnimation ();

		return go;
	}


	protected function get spawnLoc() : Vector3 {
		return  new Vector3(
			transform.position.x ,
			transform.position.y + offsetY,
			transform.position.z
		) + (transform.forward * spawnDistance);
	}

	private function fireCoroutine(meteor : GameObject) : IEnumerator {
		yield WaitForSeconds(riseTime);

		meteor.GetComponent.<Rigidbody>().isKinematic = false;
		var anim : JSPositionAnimator = meteor.AddComponent.<JSPositionAnimator> ();
		anim.autostart = false;
		anim.speed = projectileFireSpeed;
		anim.targetPosition = player.transform.position;
		anim.startAnimation ();
	}

	protected function fireProjectile(meteor : GameObject) {
		timeSinceLastShot = 0f;
		StartCoroutine(fireCoroutine(meteor));
	}

	function Update() {
		if (shouldTurn) {
			gameObject.transform.LookAt(player.transform);
		}

		super.Update();
	}

	protected function get shouldShoot() : boolean {
		return can_shoot
				&& distance <= shootDistanceThreshold
				&& timeSinceLastShot >= shootFrequency
				&& !dead
				&& !playerHealth.dead;
	}
}