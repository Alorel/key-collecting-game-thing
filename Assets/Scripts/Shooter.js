﻿#pragma strict

protected var player : GameObject;
protected var gameState : GameStateManager;

var can_shoot : boolean = true;
var offsetY : float = 0.5;
var power : int = 1000;
var projectilePrefab : GameObject;
public var shootFrequency : float = 0.5f;

private var _timeSinceLastShot : float = 0;

private var _health : HealthHaver;

function Start () {
	player = GameObject.Find("Player");
	gameState = GameObject.Find("GameState").GetComponent.<GameStateManager>();
	health = gameObject.GetComponent.<HealthHaver>();
}

public function get timeSinceLastShot() : float {
	return _timeSinceLastShot;
}

protected function get health() : HealthHaver {
	return _health;
}

private function set health(value : HealthHaver) {
	_health = value;
}

protected function set timeSinceLastShot(value : float) {
	_timeSinceLastShot = value;
}

protected function get dead() : boolean {
	return health != null && health.dead;
}

protected function get shouldShoot() : boolean {
	return can_shoot && Input.GetMouseButtonDown(0) && timeSinceLastShot >= shootFrequency && !dead;
}

protected function get spawnLoc() : Vector3 {
	return  new Vector3(
		transform.position.x,
		transform.position.y + offsetY,
		transform.position.z
	);
}

protected function instantiateProjectile() : GameObject {
	var go : GameObject = Instantiate(
		projectilePrefab, 
		spawnLoc, 
		Camera.main.transform.rotation
	) as GameObject;
	addCollisionIgnore(go);
	return go;
}

protected function fireProjectile(projectile : GameObject) {
	var rigid : Rigidbody = projectile.GetComponent.<Rigidbody>();
	rigid.AddRelativeForce(Vector3.forward * power);
	_timeSinceLastShot = 0f;
}

protected function addCollisionIgnore(projectile : GameObject) {
	var col : Collider = projectile.GetComponent.<Collider>();

	if (col != null) {
		Physics.IgnoreCollision(GetComponent.<Collider>(), col);
	}
}

function Update () {
	if (shouldShoot) {
		fireProjectile(instantiateProjectile());
	} else {
		_timeSinceLastShot += Time.deltaTime;
	}
}