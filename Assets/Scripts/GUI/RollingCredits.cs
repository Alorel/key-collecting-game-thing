﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Transform))]
public class RollingCredits : MonoBehaviour {

	private Transform rect;

	[Tooltip("The Y position will be increased if this positive or decreased if this is negative")]
	[Header("Rolling Settings")]
	public float speed = 30f;
	public bool autostart = true;

	[Header("Autostop")]
	[Tooltip("Autostop after the specified amount of seconds?")]
	public bool autostop = false;
	[Range(1f, 7200f)]
	public float autostopSeconds = 60f;

	private bool running;
	private float timeElapsed = 0f;

	// Use this for initialization
	void Start () {
		this.rect = GetComponent<Transform> ();
		running = autostart;
	}

	private void handleAutostop() {
		if (autostop && (timeElapsed += Time.deltaTime) > autostopSeconds) {
			Destroy (this);
		}
	}

	private void handleScroll() {
		this.rect.position = new Vector3 (
			this.rect.position.x,
			this.rect.position.y + (speed * Time.deltaTime),
			this.rect.position.z
		);
	}
	
	// Update is called once per frame
	void Update () {
		if (running) {
			handleAutostop ();
			handleScroll ();
		}
	}
}