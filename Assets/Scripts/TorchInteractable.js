﻿#pragma strict

var gameMessagePrefab : GameObject;

function onInteraction(rh : RaycastHit) {
	for (var go : GameObject in GameObject.FindGameObjectsWithTag("GameMessage")) {
		go.GetComponentInChildren.<PrefabManager>().hide();
	}
	Instantiate(gameMessagePrefab);

	GameObject.FindWithTag("Player").GetComponentInChildren.<Light>(true).gameObject.SetActive(true);
	Destroy(gameObject);
}